package ie.martins.luiz.journeytracker;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // ---- Text view which is changed between active and inactive depending on the GPS status ----
    static TextView tv_gps_status;

    static LocationManager locmanager;
    static LocationListener ll;
    // ---- Variable which is used to store the current speed from the GPS, that's the main variable on this app ----
    static Float float_current_speed;
    // ---- Those variables are storing the location of the device, but they are not being used. It is here for future use. ----
    static Double double_latitude;
    static Double double_longitude;

    // ---- Private method that will add a location listener to the location manager -----
    protected static void addLocationListener() {
        // ---- There are a few "checkPermission" issues on the following code extracted from the Moodle examples, that is a
        // requirement for API23, if those recommendations are applied the App doesn't work well on API 15 which is the goal
        // for this Assignment.
        locmanager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, ll = new
                LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        // ---- The location of the device has changed so update the textviews ----
                        tv_gps_status.setText(R.string.tv_gps_status_active);
                        float_current_speed = (location.getSpeed()*3600/1000); //meters/sec to km/h
                        double_latitude = location.getLatitude();
                        double_longitude = location.getLongitude();
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                        // ---- If GPS has been disabled then update the textview ----
                        if (provider.equals(LocationManager.GPS_PROVIDER)) {
                            tv_gps_status.setText(R.string.tv_gps_status_inactive);
                        }
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                        // ---- If there is a last known location then update the textview with the gps status ----
                        if (provider.equals(LocationManager.GPS_PROVIDER)) {
                            Location l =
                                    locmanager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (l != null) {
                                tv_gps_status.setText(R.string.tv_gps_status_active);
                            }
                        }
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle
                            extras) {
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // ---- Pull the textviews from the xml and get access to the location manager ----
        tv_gps_status = (TextView) findViewById(R.id.tv_gps_status);
        // ---- It adds the location listener ----
        locmanager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
